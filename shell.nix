let
  pkgs =
    import
      (builtins.fetchTarball {
        url = "https://github.com/nixos/nixpkgs/archive/d934204a0f8d9198e1e4515dd6fec76a139c87f0.tar.gz";
        sha256 = "1zfby2jsfkag275aibp81bx1g1cc305qbcy94gqw0g6zki70k1lx";
      })
      { };
in
pkgs.mkShell {
  packages = [
    pkgs.cacert
    pkgs.gitFull
    pkgs.gitlint
    pkgs.nixfmt-rfc-style
    pkgs.nodePackages.prettier
    pkgs.pre-commit
    pkgs.shellcheck
    pkgs.shfmt
    pkgs.statix
    pkgs.yq-go
  ];
}
